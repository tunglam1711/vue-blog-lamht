import {createRouter, createWebHistory} from 'vue-router';
import Search from './pages/blog/search.vue';
import Create from './pages/blog/new.vue';
import Index from './pages/blog/index.vue';
import Update from './pages/blog/_id.vue';

const routes = [
    {
        path: '/',
        name: 'index',
        component: Index,
    },
    {
        path: '/search',
        name: 'search',
        component: Search,
    },
    {
        path: '/create',
        name: 'create',
        component: Create,
    },
    {
        path: '/update/:id',
        name: 'update',
        component: Update,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes: routes
});

export default router;