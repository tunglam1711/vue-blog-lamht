export function validateBlog(blog) {
   const errors = [];
   //Required Title
   if (!blog.title || blog.title.length < 0) {
     errors.push("title");
   }
   //Required category
   if (!blog.category || blog.category.length < 0) {
     errors.push("category");
   }
   //Required position
   if (!blog.position || blog.position.length < 1) {
     errors.push("position");
   }
   //Required data_pubblic
   if (!blog.data_pubblic || blog.data_pubblic.length < 0) {
     errors.push("data_pubblic");
   }
   //Required public
   if (!blog.pubblic || blog.pubblic.length < 0) {
     errors.push("pubblic");
   }
   //Required des
   if (!blog.des || blog.des.length < 0) {
     errors.push("des");
   }
   //Required detail
   if (!blog.detail || blog.detail.length < 0) {
     errors.push("detail");
   }
 
   return errors;
 }